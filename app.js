var express = require('express');
var fs = require('fs');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var dataDir = './data/';
//var routes = require('./routes/index');
//var users = require('./routes/users');

var app = express();

var site = process.env.SITE;
var port = process.env.PORT;

// view engine setup
app.set('views', path.join('public'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('public'));

switch (site){
    case 'strechy':
		var appData = JSON.parse(fs.readFileSync(dataDir + '/nejlevnejsimytistrech.cz.json', 'utf-8'));
        break;
    case 'fasady':
		var appData = JSON.parse(fs.readFileSync(dataDir + '/nejlevnejsimytifasad.cz.json', 'utf-8'));
        break;
    default:
		var appData = JSON.parse(fs.readFileSync(dataDir + '/nejlevnejsimytistrech.cz.json', 'utf-8'));
        break;
}

app.get('/', function(req, res) {
  res.render('index', appData);
});

app.listen(port, 'localhost');
app.on('started', function() {
  console.log('Express server started on port %s at %s', server.address().port, server.address().address);
});
